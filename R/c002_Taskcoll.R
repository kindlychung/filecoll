# TODO: Add comment
# 
# Author: kaiyin
###############################################################################


Taskcoll = setRefClass("Taskcoll",
		fields = list(
                rootFiles = "character",
				rootBedFile = "character",
				rootBedStem = "character",
				rootDir = "character",
				linkedFiles = "character",
				taskBedFiles = "character",
		        taskpath = "character",
				plinkOutExt = "character"
				)
		)

		

Taskcoll$methods(
		softInit = function() {
				targetFiles = file.path(rootDir, rootFiles)
				.self$linkedFiles = file.path(taskpath, rootFiles)

				debugps("Creating the following links: ")
				debugpo(linkedFiles)
				file.symlink(targetFiles, linkedFiles)
		}
		)
	
		
		
Taskcoll$methods(
		hardInitBySnp = function(excludeSnpFile, nMaxShift) {
			targetBedStem = file.path(.self$taskpath, .self$rootBedStem)
			debugps("Instead of creating links, I am going to make a new bed file based on the exclusion list you provided: ", excludeSnpFile, "\nAt this location: ", targetBedStem)

			plinkr(bfile=.self$rootBedStem, exclude = excludeSnpFile, make_bed = "", out = targetBedStem)
			bedcollr(bfile = targetBedStem, nshift_min = 1, nshift_max = nMaxShift)
			
			.self$taskBedFiles = Sys.glob(paste(targetBedStem, "_shift_*.bed", sep=""))
			debugps("Following bed files have been generated in taskpath: ")
			debugpo(.self$taskBedFiles)
		}
		)
		
Taskcoll$methods(
		hardInitByPval = function(pvalDat, pvalThresh = 5e-2, nMaxShift) {
			debugpso("Head of pvalDat: ", head(pvalDat))
			if((! "SNP" %in% colnames(pvalDat)) | (! "P" %in% colnames(pvalDat))) {
				stop("You must provide at least two columns of data: SNP and P!")
			}

			excludeSnpVector = pvalDat$SNP[pvalDat$P > pvalThresh]
			debugpso("SNPs to be excluded: ", head(excludeSnpVector))
			excludeSnpFile = paste(.self$rootBedStem, "_exclude_by_p_", format(pvalThresh, scientific=TRUE), ".snplist", sep="")

			write.table(excludeSnpVector, file=excludeSnpFile, quote=FALSE, row.names=FALSE, col.names=FALSE)
			.self$hardInitBySnp(excludeSnpFile, nMaxShift)
		}
		)
		
Taskcoll$methods(
		hardInitByPlinkOut = function(plinkOutFile, pvalThresh = 5e-2, nMaxShift) {
			pvalDat = readplinkoutr(plinkOutFile, c("SNP", "P"))
			.self$hardInitByPval(pvalDat, pvalThresh, nMaxShift)
		}
		)
		
Taskcoll$methods(
		analyze = function(...) {
			paramVector = unlist(as.list(match.call())[-1])
			paramNames = names(paramVector)
			debugps("Parameters in Taskcall$analyze: ")
			debugpo(paramVector)
			
			if("linear" %in% paramNames) {
				.self$plinkOutExt = "assoc.linear"
			}
			else if("logistic" %in% paramNames) {
				.self$plinkOutExt = "assoc.logistic"
			}
			else {
				debugps("Unrecognized analysis type.")
				.self$plinkOutExt = ""
			}
			
			plinkcollr(..., plinkcollFileStems = .self$taskBedFiles)
			
			if((!"wait" %in% paramNames) | ("wait" %in% paramNames & paramVector["wait"] == TRUE)) {
				.self$cleanLog()
			}
		}
		)
	
Taskcoll$methods(
		cleanLog = function(ext = c("log", "nosex")) {
			for(extIter in ext) {
				filesToClean = Sys.glob(file.path(.self$taskpath, paste("*.", extIter, sep="")))
				file.remove(filesToClean)
			}
		}
		)
		
Taskcoll$methods(
		initialize = function(taskname="") {

			if(nzchar(taskname)) {
				bedFiles = Sys.glob("*.bed")
				if(length(bedFiles) < 3) {
					stop("No shifted bed files found!")
				}
				
				.self$rootBedFile = bedFiles[!grepl("_shift_", bedFiles)]
				if(length(.self$rootBedFile) > 1) {
					stop("Only one root bed file allowed!")
				}
				
				.self$rootBedStem = getstem(rootBedFile)
				.self$rootFiles = Sys.glob(paste(.self$rootBedStem, "*", sep=""))
				.self$rootDir = getwd()
				
				debugps("Files you can link into the taskpath:")
				debugpo(rootFiles)
				
				
				.self$taskpath = paste("collr_task_", taskname, sep="")
				if(file.exists(.self$taskpath)) {
					if(file.info(.self$taskpath)[, "isdir"]) {
						debugps("Using existing taskpath: ", taskpath)
					}
					else {
						stop(paste("You have a file named ", taskpath, "which conflicts with your taskname. Either rename the file or change the taskname."))
					}
				}
				else {
					debugps("Creating new taskpath: ", taskpath)
					dir.create(taskpath)
				}			
			}
			else {
				debugps("No taskname provided!")
			}
		}
		)
		
.DollarNames.Taskcoll <- function(x, pattern){
        grep(pattern, c(getRefClass(class(x))$methods(), names(getRefClass(class(x))$fields())), value=TRUE, perl=TRUE)
}