# TODO: Add comment
# 
# Author: kaiyin
###############################################################################



getConfig = function() {
	sysname = Sys.info()["sysname"]
	if(sysname == "Darwin" | sysname == "Linux") {
		homeDir = Sys.getenv("HOME")
		bedcollConfigFile = file.path(homeDir, ".collrrc")
	} else {
		homeDir = Sys.getenv("HOMEPATH")
		bedcollConfigFile = file.path(homeDir, "_collrrc")
	}
	configTable = read.table(file = bedcollConfigFile, header = FALSE, sep=",", row.names = NULL, stringsAsFactors=FALSE)
	config = configTable[, 2]
	names(config) = configTable[, 1]
	config
}


